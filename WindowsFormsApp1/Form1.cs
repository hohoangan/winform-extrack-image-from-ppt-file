﻿using DocumentFormat.OpenXml.Packaging;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Aspose.Slides.Export;
using Aspose.Slides;
using System.ComponentModel;
using System.Threading;
using System.Net;
using System.Text;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        BackgroundWorker _backgroundWorker = new BackgroundWorker();
        BackgroundWorker _load = new BackgroundWorker();
        bool loading = false;
        public Form1()
        {
            InitializeComponent();
            // Set up the Background Worker Events 
            _backgroundWorker.DoWork += _backgroundWorker_DoWork;
            _backgroundWorker.RunWorkerCompleted += _backgroundWorker_RunWorkerCompleted;

            _load.DoWork += _load_DoWork; ;
            _load.RunWorkerCompleted += _load_RunWorkerCompleted;
        }

        private void _load_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _load.RunWorkerAsync();
        }

        private void _load_DoWork(object sender, DoWorkEventArgs e)
        {
            if (loading == false)
                return;
            MethodInvoker methodInvokerDelegate = delegate ()
            {
                string[] str = { "-", "\\", "|", "/", "-" };
                for (int i = 0; i < str.Length; i++)
                {
                    //add text
                    richTextBox1.Select(richTextBox1.TextLength - 1, 0);
                    richTextBox1.AppendText(str[i]);
                    //remove cái cũ
                    richTextBox1.SelectionStart = richTextBox1.TextLength - 1;
                    richTextBox1.SelectionLength = 1;
                    richTextBox1.SelectionColor = Color.Red;
                    richTextBox1.SelectedText = "";
                    Thread.Sleep(200);
                }
            };

            //This will be true if Current thread is not UI thread.
            if (this.InvokeRequired)
                this.Invoke(methodInvokerDelegate);
            else
                methodInvokerDelegate();
        }

        void ChangeTextRun(string name)
        {
            MethodInvoker methodInvokerDelegate = delegate ()
            {
                btnRun.Text = name;
            };

            //This will be true if Current thread is not UI thread.
            if (this.InvokeRequired)
                this.Invoke(methodInvokerDelegate);
            else
                methodInvokerDelegate();
        }

        private void _backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnRun.Enabled = true;
            ChangeTextRun("RUN ppt | pttx | pdf");
        }

        private void _backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            MethodInvoker methodInvokerDelegate = delegate ()
            {
                btnRun.Enabled = false;
            };

            //This will be true if Current thread is not UI thread.
            if (this.InvokeRequired)
                this.Invoke(methodInvokerDelegate);
            else
                methodInvokerDelegate();
            Execute();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string folderPath = "";
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                folderPath = folderBrowserDialog1.SelectedPath;
                txtPath.Text = folderPath;
            }
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            if (!_backgroundWorker.IsBusy)
                _backgroundWorker.RunWorkerAsync();
        }

        void Execute()
        {
            try
            {
                UpdateForm("-------------BEGIN-------------", Color.Green, true);
                List<string> filePaths = Directory.GetFiles(txtPath.Text.Trim(), "*.*",
                                         SearchOption.TopDirectoryOnly)
                                         .Where(s => s.EndsWith(".pptx") || s.EndsWith(".ppt") || s.EndsWith(".pdf")).ToList();
                int idx = 0;
                filePaths.ForEach(item =>
                {
                    ChangeTextRun(string.Format("{0}/{1}", ++idx, filePaths.Count));
                    var filename = Path.GetFileNameWithoutExtension(item);
                    var extension = Path.GetExtension(item);
                    var folder = Path.Combine(txtPath.Text, filename);
                    var destFIleName = Path.Combine(folder, filename + ".pptx");

                    if (extension.Equals(".ppt") || extension.Equals(".pptx") || extension.Equals(".pdf"))
                    {
                        UpdateForm(string.Format("{0} : ", item), Color.Black, true);
                        loading = true;
                        if (!_load.IsBusy)
                            _load.RunWorkerAsync();
                        System.IO.Directory.CreateDirectory(folder);
                        try
                        {
                            if (extension.Equals(".ppt"))
                            {
                                var fileOld = item;
                                // Instantiate a Presentation object that represents the PPT file
                                Presentation pres = new Presentation(item);
                                item = Path.Combine(txtPath.Text, filename + ".pptx");
                                // Save the PPT presentation as PPTX
                                pres.Save(item, SaveFormat.Pptx);

                                //save file goc
                                var fileOrg = Path.Combine(folder, filename + ".ppt");

                                //MOVE
                                if (File.Exists(fileOrg))
                                    File.Delete(fileOrg);
                                File.Move(fileOld, fileOrg);
                                if (File.Exists(destFIleName))
                                    File.Delete(destFIleName);
                                File.Move(item, destFIleName);
                                GetImages(destFIleName, folder);
                                File.Delete(destFIleName);
                            }
                            else if (extension.Equals(".pptx"))
                            {
                                if (File.Exists(destFIleName))
                                    File.Delete(destFIleName);
                                File.Move(item, destFIleName);
                                GetImages(destFIleName, folder);
                            }
                            else if (extension.Equals(".pdf"))
                            {
                                pdf(item, folder);
                                destFIleName = Path.Combine(folder, filename + ".pdf");
                                if (File.Exists(destFIleName))
                                    File.Delete(destFIleName);
                                File.Move(item, destFIleName);
                            }
                            loading = false;
                            UpdateForm("Done", Color.Green);
                        }
                        catch (Exception ex)
                        {
                            loading = false;
                            UpdateForm(ex.Message, Color.Red, true);
                        }
                    }
                });
                UpdateForm("--------------END-------------", Color.Green, true);
            }
            catch (Exception ex)
            {
                UpdateForm(ex.Message, Color.Red, true);
            }
        }

        public static void GetImages(string filename, string path)
        {
            var doc = PresentationDocument.Open(filename, true);
            var presentationPart = doc.PresentationPart;
            var list = presentationPart.GetPartsOfType<SlidePart>();
            //foreach (var slide in presentationPart.GetPartsOfType<SlidePart>())
            foreach (var slide in list)
            {
                int stt = 1;
                var index = Path.GetFileNameWithoutExtension(slide.Uri.OriginalString);
                foreach (var image in slide.GetPartsOfType<ImagePart>())
                {
                    if (image != null)
                    {
                        var stream = image.GetStream();
                        var img = Image.FromStream(stream);
                        XuLyAnh(img, path, index, stt);
                        stt++;
                        /*
                        //var w = img.Width;
                        //var h = img.Height;
                        //if (w >= 720 && h != 1251)
                        //{
                        //    var name = (stt++).ToString();
                        //    var ratio = Math.Round((double)w / h, 2);
                        //    if (ratio >= 1.8 || ratio <= 1.7)
                        //    {
                        //        var newW = 0;
                        //        var newH = 0;
                        //        if (w / 1.77 <= h)
                        //        {
                        //            newW = w;
                        //            newH = (int)(w / 1.77) > h ? h : (int)(w / 1.77);
                        //        }
                        //        else if (h * 1.77 <= w)
                        //        {
                        //            newW = (int)(w * 1.77) > w ? w : (int)(w * 1.77);
                        //            newH = h;
                        //        }
                        //        //cut
                        //        Bitmap src = new Bitmap(img);
                        //        Bitmap target = src.Clone(new Rectangle(0, 0, newW, newH), src.PixelFormat);
                        //        //save
                        //        //img.Save(Path.Combine(path, System.IO.Path.ChangeExtension(System.IO.Path.GetRandomFileName(), "jpg")));
                        //        //save
                        //        Bitmap resized = new Bitmap(target, new Size(1920, 1080));
                        //        resized.SetResolution(72, 72);
                        //        SaveJpeg(Path.Combine(path, System.IO.Path.ChangeExtension(index + "_" + name, "jpg")), resized, 80);
                        //        //resized.Save(Path.Combine(path, System.IO.Path.ChangeExtension(index +"_"+ name, "jpg")));
                        //    }
                        //    else
                        //    {
                        //        //save
                        //        Bitmap resized = new Bitmap(img, new Size(1920, 1080));
                        //        resized.SetResolution(72, 72);
                        //        SaveJpeg(Path.Combine(path, System.IO.Path.ChangeExtension(index + "_" + name, "jpg")), resized, 80);
                        //        //resized.Save(Path.Combine(path, System.IO.Path.ChangeExtension(index + "_" + name, "jpg")));
                        //    }
                        //}
                        */
                    }
                }
            }
            doc.Close();
        }

        public static void SaveJpeg(string path, System.Drawing.Image img, int quality)
        {
            if (quality < 0 || quality > 100)
                throw new ArgumentOutOfRangeException("quality must be between 0 and 100.");

            // Encoder parameter for image quality 
            EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
            // JPEG image codec 
            ImageCodecInfo jpegCodec = GetEncoderInfo("image/jpeg");
            EncoderParameters encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;
            img.Save(path, jpegCodec, encoderParams);
        }
        private static ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            // Get image codecs for all image formats 
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

            // Find the correct image codec 
            for (int i = 0; i < codecs.Length; i++)
                if (codecs[i].MimeType == mimeType)
                    return codecs[i];

            return null;
        }

        public static void pdf(string fileName, string path)
        {
            //Instantiate an object of Spire.Pdf.PdfDocument
            Spire.Pdf.PdfDocument doc = new Spire.Pdf.PdfDocument();
            //Load a PDF file
            doc.LoadFromFile(fileName);
            List<Image> ListImage = new List<Image>();

            for (int i = 0; i < doc.Pages.Count; i++)
            {
                // Get an object of Spire.Pdf.PdfPageBase
                Spire.Pdf.PdfPageBase page = doc.Pages[i];
                // Extract images from Spire.Pdf.PdfPageBase
                Image[] images = page.ExtractImages();
                if (images != null && images.Length > 0)
                {
                    for (int j = 0; j < images.Length; j++)
                    {
                        Image img = images[j];
                        XuLyAnh(img, path, (i + 1).ToString(), j + 1);
                    }
                }
            }
        }

        public static void XuLyAnh(Image img, string path, string pageIndex, int name)
        {
            var w = img.Width;
            var h = img.Height;
            if (w >= 200 && h != 1251)
            {
                var ratio = Math.Round((double)w / h, 2);
                if (ratio >= 2.5)
                    return;
                if (ratio >= 1.8 || ratio <= 1.7)
                {
                    var newW = 0;
                    var newH = 0;
                    if (w / 1.77 <= h)
                    {
                        newW = w;
                        newH = (int)(w / 1.77) > h ? h : (int)(w / 1.77);
                    }
                    else if (h * 1.77 <= w)
                    {
                        newW = (int)(w * 1.77) > w ? w : (int)(w * 1.77);
                        newH = h;
                    }
                    //cut
                    Bitmap src = new Bitmap(img);
                    Bitmap target = src.Clone(new Rectangle(0, 0, newW, newH), src.PixelFormat);
                    //save
                    //img.Save(Path.Combine(path, System.IO.Path.ChangeExtension(System.IO.Path.GetRandomFileName(), "jpg")));
                    //save
                    Bitmap resized = new Bitmap(target, new Size(1920, 1080));
                    resized.SetResolution(72, 72);
                    SaveJpeg(Path.Combine(path, Path.ChangeExtension(string.Format("page{0}_img{1}", pageIndex, name), "jpg")), resized, 80);
                    //resized.Save(Path.Combine(path, System.IO.Path.ChangeExtension(index +"_"+ name, "jpg")));
                }
                else
                {
                    //save
                    Bitmap resized = new Bitmap(img, new Size(1920, 1080));
                    resized.SetResolution(72, 72);
                    SaveJpeg(Path.Combine(path, Path.ChangeExtension(string.Format("page{0}_img{1}", pageIndex, name), "jpg")), resized, 80);
                    //resized.Save(Path.Combine(path, System.IO.Path.ChangeExtension(index + "_" + name, "jpg")));
                }
            }
        }
        void UpdateForm(string content, Color color, bool newrow = false, double percent = 0)
        {
            MethodInvoker methodInvokerDelegate = delegate ()
            {
                if (newrow == true)
                    richTextBox1.AppendText("\r\n");

                richTextBox1.SelectionStart = richTextBox1.TextLength;
                richTextBox1.SelectionLength = 0;
                richTextBox1.SelectionColor = color;
                richTextBox1.AppendText(content);
                richTextBox1.SelectionColor = richTextBox1.ForeColor;

                richTextBox1.ScrollToCaret();
            };

            //This will be true if Current thread is not UI thread.
            if (this.InvokeRequired)
                this.Invoke(methodInvokerDelegate);
            else
                methodInvokerDelegate();
        }

        private void btnReadFolderName_Click(object sender, EventArgs e)
        {
            string root = txtPath.Text.Trim();
            // Get all subdirectories
            List<string> subdirectoryEntries = Directory.GetDirectories(root).ToList();
            subdirectoryEntries.ForEach(x =>
            {
                FileInfo f = new FileInfo(x);
                UpdateForm(f.Name, Color.Black, true);
            });
        }

        #region get price
        void ReadDateFromUrl(string url)
        {
            string urlAddress = url;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlAddress);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = null;
                if (String.IsNullOrWhiteSpace(response.CharacterSet))
                    readStream = new StreamReader(receiveStream);
                else
                    readStream = new StreamReader(receiveStream,
                        Encoding.GetEncoding(response.CharacterSet));
                string data = readStream.ReadToEnd();
                response.Close();
                readStream.Close();
            }
        }
        #endregion

        private void btngetprice_Click(object sender, EventArgs e)
        {
            ReadDateFromUrl("https://docs.google.com/presentation/d/1m_pQRmfnH_BUlgvybHJwH8fYfOrYNwPu/edit#slide=id.p1");
        }
    }
}
