﻿
namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPath = new System.Windows.Forms.TextBox();
            this.btnBrowser = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.btnReadFolderName = new System.Windows.Forms.Button();
            this.btngetprice = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(12, 23);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(357, 20);
            this.txtPath.TabIndex = 0;
            // 
            // btnBrowser
            // 
            this.btnBrowser.Location = new System.Drawing.Point(389, 21);
            this.btnBrowser.Name = "btnBrowser";
            this.btnBrowser.Size = new System.Drawing.Size(98, 23);
            this.btnBrowser.TabIndex = 1;
            this.btnBrowser.Text = "Chọn thư mục";
            this.btnBrowser.UseVisualStyleBackColor = true;
            this.btnBrowser.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnRun
            // 
            this.btnRun.Location = new System.Drawing.Point(493, 21);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(135, 23);
            this.btnRun.TabIndex = 2;
            this.btnRun.Text = "RUN ppt | pttx | pdf";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.Location = new System.Drawing.Point(12, 49);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(725, 388);
            this.richTextBox1.TabIndex = 3;
            this.richTextBox1.Text = "";
            // 
            // btnReadFolderName
            // 
            this.btnReadFolderName.Location = new System.Drawing.Point(634, 20);
            this.btnReadFolderName.Name = "btnReadFolderName";
            this.btnReadFolderName.Size = new System.Drawing.Size(93, 23);
            this.btnReadFolderName.TabIndex = 4;
            this.btnReadFolderName.Text = "Đọc tên folder";
            this.btnReadFolderName.UseVisualStyleBackColor = true;
            this.btnReadFolderName.Click += new System.EventHandler(this.btnReadFolderName_Click);
            // 
            // btngetprice
            // 
            this.btngetprice.Location = new System.Drawing.Point(756, 19);
            this.btngetprice.Name = "btngetprice";
            this.btngetprice.Size = new System.Drawing.Size(75, 23);
            this.btngetprice.TabIndex = 5;
            this.btngetprice.Text = "Lấy giá XLS";
            this.btngetprice.UseVisualStyleBackColor = true;
            this.btngetprice.Click += new System.EventHandler(this.btngetprice_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(878, 449);
            this.Controls.Add(this.btngetprice);
            this.Controls.Add(this.btnReadFolderName);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.btnRun);
            this.Controls.Add(this.btnBrowser);
            this.Controls.Add(this.txtPath);
            this.Name = "Form1";
            this.Text = "CREATE FOLDER FROM PTTX";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Button btnBrowser;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button btnReadFolderName;
        private System.Windows.Forms.Button btngetprice;
    }
}

